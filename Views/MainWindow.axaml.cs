using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using NAudio.Wave;

namespace SlimeGame.Views;

public partial class MainWindow : Window
{
    private HashSet<Key> PressedKeys { get;} = new();
    private DispatcherTimer? timer;
    private WaveOutEvent waveOut;
    private AudioFileReader? audioFileReader;
    
    public MainWindow()
    {
        InitializeComponent();
        Character = this.FindControl<Image>("Character");
        
        waveOut = new WaveOutEvent();
        
        KeyDown += HandleKeyDown;
        KeyUp += HandleKeyUp;
    }
    
    private void HandleKeyDown(object? sender, KeyEventArgs e)
    {
        PressedKeys.Add(e.Key);
        timer?.Stop();
        MovePlayer(sender, e);
    }

    private void HandleKeyUp(object? sender, KeyEventArgs e)
    {
        PressedKeys.Remove(e.Key);
        timer?.Stop();
    }

    public void MovePlayer(object sender, KeyEventArgs  e)
    {
        switch (e.Key)
        {
            case Key.Down:
                SlimeDown();
                break;
            case Key.S:
                SlimeDown();
                break;
            case Key.Up:
                SlimeJump();
                break;
            case Key.W:
                SlimeJump();
                break;
        }
        
    }

    public async void SlimeDown()
    {
        
        LoadAudio("../../../Assets/Sounds/downSound.mp3");
        waveOut.Play();
        Character.Source = new Bitmap($"../../../Assets/Image/slimeDown.png");
            
        await Task.Delay(160);
        
        waveOut.Stop();
        Character.Source = new Bitmap("../../../Assets/Image/slimeNormal.png");
    }

    public async void SlimeJump()
    {
        LoadAudio(path:"../../../Assets/Sounds/jumpSound.mp3");
        waveOut.Play();
        Character.Source = new Bitmap("../../../Assets/Image/slimeJump.png");
        Canvas.SetTop(Character, 100);

        await Task.Delay(160);
        
        waveOut.Stop();
        Character.Source = new Bitmap("../../../Assets/Image/slimeNormal.png");
        Canvas.SetTop(Character, 200);
    }
    
    public void LoadAudio(string path)
    {
        string audioFilePath = path;

        if (waveOut.PlaybackState == PlaybackState.Playing)
        {
            waveOut.Stop();
        }

        audioFileReader = new AudioFileReader(audioFilePath);
        waveOut.Init(audioFileReader);
    }

    
}